import exceptions.NameIsNullException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GreetingTest {
    @Test
    void shouldReturnASimpleGreetingWhenNameIsPassed() throws NameIsNullException {
        Greeting greeting=new Greeting();

        String greetingText=greeting.greet("Bob");

        assertThat(greetingText,is(equalTo("Hello, Bob")));
    }

    @Test
    void shouldReturnHelloMyFriendWhenNameIsNull() throws NameIsNullException {
        NameIsNullException thrown=assertThrows(NameIsNullException.class, () -> {
            Greeting greeting = new Greeting();
            greeting.greet(null);
        });
        Assertions.assertEquals("Hello, my friend", thrown.getMessage());

    }

    @Test
    void shouldShoutBackToUserWhenNameIsInUpperCase() throws NameIsNullException {
        Greeting greeting = new Greeting();

        String greetingText = greeting.greet("JERRY");

        assertThat(greetingText,is(equalTo("HELLO JERRY!")));
    }
}
