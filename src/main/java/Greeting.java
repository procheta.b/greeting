import exceptions.NameIsNullException;

import java.util.Locale;

import static java.lang.Character.isUpperCase;

public class Greeting {
    private String greeting;

    private boolean isaBoolean(String name) {
        return name == name.toUpperCase(Locale.ROOT);
    }

    public Greeting()
    {
        this.greeting="";
    }

    public String greet(String name) throws NameIsNullException {
        if(name==null) throw new NameIsNullException();
        if (isaBoolean(name))
        {
            this.greeting="HELLO "+name+"!";
        }
        else {
            this.greeting="Hello, "+name;
        }
        return greeting;
    }



}
